/* See LICENSE file for copyright and license details. */

/* fonts */
# include "fonts/mytheme.h"

/* appearance */
static const unsigned int borderpx       = 2;   /* border pixel of windows */
static const unsigned int gappx          = 7;  /* gaps between windows */
static const unsigned int snap           = 32;  /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft  = 0;  	/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray             = 1;   /* 0 means no systray */
static const int showbar                 = 1;   /* 0 means no bar */
static const int topbar                  = 1;   /* 0 means bottom bar */
static const int user_bh                 = 23;   /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const int focusonwheel            = 0;

/* colors */
static char normfgcolor[]         = "#bbbbbb";
static char normbgcolor[]         = "#222222";
static char normbordercolor[]     = "#111111";
static char selfgcolor[]          = "#eeeeee";
static char selbgcolor[]          = "#535D6C";
static char selbordercolor[]      = "#535D6C";
static char titlefgcolor[]        = "#eeeeee";
static char titlebgcolor[]        = "#535D6C";
static char titlebordercolor[]    = "#535D6C";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
       [SchemeTitle]  = { titlefgcolor,  titlebgcolor,  titlebordercolor  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class             instance    title           tags mask     iscentered   isfloating   monitor */
	{ "librewolf",       NULL,       NULL,           1 << 8,       0,           0,           -1 },
	{ "Firefox",         NULL,       NULL,           1 << 8,       0,           0,           -1 },
	{ "Gimp",            NULL,       NULL,           0,            0,           1,           -1 },
	{ "Gcr-prompter",    NULL,       NULL,           0,       	   1,           1,        	 -1 },
	{ "Galculator",      NULL,       NULL,           0,       	   1,           1,        	 -1 },
	{ "Lxappearance",    NULL,       NULL,           0,       	   1,           1,        	 -1 },
	{ "Kvantum Manager", NULL,       NULL,           0,       	   1,           1,        	 -1 },
	{ "qt5ct",           NULL,       NULL,           0,       	   1,           1,        	 -1 },
	{ "Mugshot",         NULL,       NULL,           0,       	   1,           1,        	 -1 },
	{ "Pavucontrol",     NULL,       NULL,           0,       	   1,           1,        	 -1 },
	{ "Gpick",           NULL,       NULL,           0,       	   1,           1,        	 -1 },
	{ "Gcolor2",         NULL,       NULL,           0,       	   1,           1,        	 -1 },
	{ "Gcolor3",         NULL,       NULL,           0,       	   1,           1,        	 -1 },
	{ "Surf",            NULL,       NULL,           0,       	   1,           1,        	 -1 },
	{ "St",              NULL,       "pulsemixer",   0,       	   1,           1,        	 -1 },
	{ "St",              NULL,       "bluetoothctl", 0,       	   1,           1,        	 -1 },
	{ "qutebrowser",     NULL,       NULL,           0,       	   1,           1,        	 -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define XF86Tools		            0x1008ff81
#define XF86MonBrightnessUp         0x1008ff02
#define XF86MonBrightnessDown       0x1008ff03
#define XF86AudioMute		        0x1008ff12
#define XF86AudioLowerVolume	    0x1008ff11
#define XF86AudioRaiseVolume	    0x1008ff13
#define XF86AudioMicMute            0x1008ffb2
#define XF86AudioPrev               0x1008ff16
#define XF86AudioPlay               0x1008ff14
#define XF86AudioNext               0x1008ff17
#define XF86Mail		            0x1008ff19
#define XF86HomePage		        0x1008ff18
#define XF86Explorer		        0x1008ff5d
#define XF86Calculator		        0x1008ff1d
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char       *dmenucmd[] = { "dmenu_run", "-g", "3", "-l", "25", "-x", "120", "-y", "120", "-z", "1100", "-p", "RUN:", "-m", dmenumon, NULL };
static const char        *roficmd[] = { "rofi", "-show", "drun", "-show-icons", NULL };
static const char        *termcmd[] = { "bash", "dmenu-terminal-selection.sh", NULL };
static const char *filemanagercmd[] = { "bash", "dmenu-filemanagers.sh", NULL };
static const char         *webcmd[] = { "bash", "dmenu-browser-selection.sh", NULL };
static const char  *calculatorcmd[] = { "galculator", NULL };
static const char       *ytubecmd[] = { "bash", "url2mpv.sh", NULL };
static const char  *radiostrmscmd[] = { "bash", "dmenustreams.sh", NULL };
static const char       *mediacmd[] = { "bash", "dmenu-players-selection.sh", NULL };
static const char       *emailcmd[] = { "thunderbird", NULL };
static const char    *settingscmd[] = { "lxappearance", NULL };
static const char         *vimcmd[] = { "st", "-e", "vim", NULL };
static const char    *sndmixercmd[] = { "st", "-e", "pulsemixer", NULL };
static const char   *bluetoothcmd[] = { "st", "-e", "bluetoothctl", NULL };
static const char     *cmdsoundup[] = { "amixer", "-D", "pulse", "-q", "sset", "Master", "2%+", NULL };
static const char   *cmdsounddown[] = { "amixer", "-D", "pulse", "-q", "sset", "Master", "2%-", NULL };
static const char *cmdsoundtoggle[] = { "amixer", "-D", "pulse", "-q", "sset", "Master", "toggle", NULL };
static const char   *cmdmictoggle[] = { "amixer", "set", "Capture", "toggle", NULL };
static const char        *brupcmd[] = { "xbacklight", "-inc", "10", NULL};
static const char      *brdowncmd[] = { "xbacklight", "-dec", "10", NULL};
static const char     *actionscmd[] = { "bash", "dmenu-menu-actions-dwm.sh", NULL };
static const char      *logoffcmd[] = { "bash", "dmenu-dwmlogoff.sh", NULL };
static const char      *rebootcmd[] = { "bash", "dmenu-reboot.sh", NULL };
static const char    *poweroffcmd[] = { "bash", "dmenu-poweroff.sh", NULL };
static const char *lockdisplaycmd[] = { "i3lock", "-c", "000000", NULL };

#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ 0,	                     XF86Tools,      spawn,          {.v = settingscmd } },
	{ 0,	         XF86MonBrightnessDown,      spawn,          {.v = brdowncmd } },
	{ 0,	           XF86MonBrightnessUp,      spawn,          {.v = brupcmd } },
	{ 0,	                 XF86AudioMute,      spawn,          {.v = cmdsoundtoggle } },
	{ 0,	              XF86AudioMicMute,      spawn,          {.v = cmdmictoggle } },
	{ 0,	          XF86AudioRaiseVolume,      spawn,          {.v = cmdsoundup } },
	{ 0,	          XF86AudioLowerVolume,      spawn,          {.v = cmdsounddown } },
	{ 0,	                      XF86Mail,      spawn,          {.v = emailcmd } },
	{ 0,	                  XF86HomePage,      spawn,          {.v = filemanagercmd } },
	{ 0,	                  XF86Explorer,      spawn,          {.v = webcmd } },
	{ 0,	                XF86Calculator,      spawn,          {.v = calculatorcmd } },

	{ ALTKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY|ShiftMask,             XK_w,      spawn,          {.v = webcmd } },
	{ MODKEY|ShiftMask,             XK_p,      spawn,          {.v = mediacmd } },
	{ MODKEY|ShiftMask,             XK_f,      spawn,          {.v = filemanagercmd } },
	{ MODKEY|ShiftMask,             XK_a,      spawn,          {.v = sndmixercmd } },
	{ MODKEY|ShiftMask,             XK_b,      spawn,          {.v = bluetoothcmd } },
	{ MODKEY|ShiftMask,             XK_e,      spawn,          {.v = emailcmd } },
	{ MODKEY|ShiftMask,             XK_v,      spawn,          {.v = vimcmd } },
	{ MODKEY|ShiftMask,             XK_y,      spawn,          {.v = ytubecmd } },
	{ MODKEY|ShiftMask,             XK_r,      spawn,          {.v = radiostrmscmd } },
	{ ALTKEY|ControlMask,           XK_Delete, spawn,          {.v = lockdisplaycmd } },

	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_p,      spawn,          {.v = roficmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.01} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.01} },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_F5,     xrdb,           {.v = NULL } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY,                       XK_x,      spawn,          {.v = actionscmd } },
	{ MODKEY|ShiftMask,             XK_q,      spawn,          {.v = logoffcmd } },
	{ MODKEY|ShiftMask,             XK_Delete, spawn,          {.v = rebootcmd } },
	{ MODKEY|ShiftMask,             XK_x,      spawn,          {.v = poweroffcmd } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

