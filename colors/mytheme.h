static const char col_gray1[]       = "#1E1E1E";
static const char col_gray2[]       = "#000000";
static const char col_gray3[]       = "#CCCCCC";
static const char col_gray4[]       = "#CCCCCC";
static const char col_cyan[]        = "#5B85B5";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};
