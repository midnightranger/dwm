static const char col_gray1[]       = "#1E1E1E"; /* Normal backgound */
static const char col_gray2[]       = "#000000";
static const char col_gray3[]       = "#CCCCCC";
static const char col_gray4[]       = "#CCCCCC";
static const char col_cyan[]        = "#0183a5"; /* Selection backgound */
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};
