This is my personal build of 'dwm', the dynamic tiling window manager from suckless.org Team. The patches I have applied are the below:

1. attachbottom: New clients attach at the bottom of the stack instead of the top.
2. autostart: This patch will make dwm run "~/.dwm/autostart_blocking.sh" and "~/.dwm/autostart.sh &" before entering the handler loop.
3. bar height: This patch allows user to change dwm's default bar height.
4. center: Add an iscentered rule to automatically center clients on the current monitor.
5. fixborders: This patch can be found in alpha patch page. When terminal has transparency then its borders also become transparent. Fix it by removing transparency from any pixels drawn.
6. focusonclick: Switch focus only by mouse click and not sloppy.
7. fullgaps: This patch adds gaps between client windows.
8. hide vacant tags: This patch prevents dwm from drawing tags with no clients (i.e. vacant) on the bar.
9. movestack: This plugin allows you to move clients around in the stack and swap them with the master.
10. pertag: This patch keeps layout, mwfact, barpos and nmaster per tag.
11. systray: A simple system tray implementation. Multi-monitor is also supported. The tray follows the selected monitor.
12. titlecolor: Adds a new color scheme used by the window title in the bar, so that its colors (foreground and background) can be changed independently.
13. xrdb: Allows dwm to read colors from xrdb (.Xresources) at run time.
